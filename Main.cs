﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;

namespace SharpNav.DungeonNavGen
{
    public class DungeonGen : AOPluginEntry
    {
        public static void Start(NavMeshGenerationSettings genSettings)
        {
            Chat.WriteLine("Starting DungeonGen");

            if (!SMovementController.IsLoaded())
            {
                Chat.WriteLine("SMovementController must be set for DungeonNavFactory to start");
                return;
            }

            DungeonNavFactory.Start(MissionMode.UnicornHubShip, genSettings);
        }

        public static void Start()
        {
            var genSettings = NavMeshGenerationSettings.MediumDensity;
            genSettings.FilterLargestSection = false;
            genSettings.AgentRadius = 1f;
            Start(genSettings);
        }
    }
}