﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using SharpNav.Geometry;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Vector3 = SharpNav.Geometry.Vector3;

namespace SharpNav.DungeonNavGen
{
    public class DungeonNavFactory
    {
        public static Action<int> OnNavmeshLoad;
        private int _oldFloor = int.MaxValue;
        private ReadOnlyDictionary<string, List<Triangle3>> _meshData;
        private DungeonData _dungeonData;
        private AutoResetInterval _updateInterval;
        private NavMeshGenerationSettings _genSettings;

        internal static void Start(MissionMode mode, NavMeshGenerationSettings genSettings)
        {
            DungeonNavFactory fac = new DungeonNavFactory(genSettings);
            fac.InternalStart(mode);
        }

        internal DungeonNavFactory(NavMeshGenerationSettings genSettings)
        {
            _genSettings = genSettings;
        }

        private void InternalStart(MissionMode mode)
        {
            MeshDataSerializer serializer = new MeshDataSerializer();

            switch (mode)
            {
                case MissionMode.UnicornHubShip:
                    _meshData = serializer.Deserialize(DungeonMeshData.UnicornOutpostMission);
                    break;
                default:
                    Chat.WriteLine("Unsupported mission type");
                    break;
            }

            _updateInterval = new AutoResetInterval(100);

            Network.N3MessageReceived += OnMessageReceived;
            Game.PlayfieldInit+= OnPlayfieldInit;
            Game.OnUpdate += OnUpdate;
        }

        internal void Stop()
        {
            Network.N3MessageReceived -= OnMessageReceived;
            Game.OnUpdate -= OnUpdate;
        }

        private void OnPlayfieldInit(object sender, uint e)
        {
            _dungeonData = null;
            _oldFloor = int.MaxValue;
        }

        private void OnUpdate(object sender, float e)
        {
            if (!Playfield.IsDungeon)
                return;

            if (!_updateInterval.Elapsed)
                return;

            if (!DynelManager.LocalPlayer.IsInPlay)
                return;

            var playerFloor = DynelManager.LocalPlayer.Room.Floor;

            if (_oldFloor == playerFloor)
                return;

            if (_dungeonData == null || _dungeonData.Identity != Playfield.ModelIdentity.Instance)
            {
                GenMeshData();
                return;
            }

            if (!_dungeonData.Navmeshes.TryGetValue(playerFloor, out NavMesh navMesh))
                return;

            SMovementController.LoadNavmesh(navMesh, true);
            OnNavmeshLoad?.Invoke(playerFloor);

            _oldFloor = playerFloor;
        }

        private void OnMessageReceived(object sender, N3Message e)
        {
            if (!Playfield.IsDungeon)
                return;

            if (e is CharInPlayMessage charInPlay && charInPlay.Identity == DynelManager.LocalPlayer.Identity)
            {
                GenMeshData();
            }
            else if (e is GenericCmdMessage cmdMsg && cmdMsg.User == DynelManager.LocalPlayer.Identity && cmdMsg.Target.Type == IdentityType.Terminal)
            {
                if (_dungeonData == null || _dungeonData.Identity != Playfield.ModelIdentity.Instance)
                    return;

                if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Level))
                    return;

                SMovementController.UnloadNavmesh();
            }
        }

        private void GenMeshData()
        {
            Dictionary<int, List<Triangle3>> preGenMesh = new Dictionary<int, List<Triangle3>>();

            foreach (var room in Playfield.Rooms)
            {
                if (!_meshData.TryGetValue(room.Name, out var tris))
                {
                    Chat.WriteLine($"Missing room {room.Name}");
                    continue;
                }

                List<Triangle3> trisCopy = new List<Triangle3>();

                foreach (var tri in tris)
                {
                    var roomCenter = RoomGeoUtils.GetCenter(room);

                    Vector3 vA = Vector3.Rotate(tri.A, Vector3.UnitY, room.Rotation);
                    Vector3 vB = Vector3.Rotate(tri.B, Vector3.UnitY, room.Rotation);
                    Vector3 vC = Vector3.Rotate(tri.C, Vector3.UnitY, room.Rotation);

                    vA += roomCenter;
                    vB += roomCenter;
                    vC += roomCenter;

                    trisCopy.Add(new Triangle3(vA, vB, vC));
                }

                if (!preGenMesh.ContainsKey(room.Floor))
                    preGenMesh.Add(room.Floor, new List<Triangle3>());

                preGenMesh[room.Floor].AddRange(trisCopy);
            }

            _dungeonData = new DungeonData
            {
                Identity = Playfield.ModelIdentity.Instance,
                Navmeshes = new Dictionary<int, NavMesh>()
            };

            foreach (var mesh in preGenMesh)
            {
                _dungeonData.Navmeshes.Add(mesh.Key, SNavMeshGenerator.GenerateFromTris(_genSettings, mesh.Value));
            }
        }

        private class DungeonData
        {
            public int Identity;
            public Dictionary<int, NavMesh> Navmeshes;
        }
    }
}

internal enum MissionMode
{
    Rubika,
    Shadowlands,
    UnicornHubShip
}