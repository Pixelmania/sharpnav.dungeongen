﻿using SharpNav.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace SharpNav.DungeonNavGen
{
    internal class MeshDataSerializer
    {
        internal void Serialize(string filePath, Dictionary<string, List<Triangle3>> allGeo)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            using (BinaryWriter writer = new BinaryWriter(fs))
            {
                writer.Write(allGeo.Count);

                foreach (var kvp in allGeo)
                {
                    writer.Write(kvp.Key);
                    writer.Write(kvp.Value.Count);

                    foreach (var tri in kvp.Value)
                    {
                        WriteVector3(writer, tri.A);
                        WriteVector3(writer, tri.B);
                        WriteVector3(writer, tri.C);
                    }
                }
            }
        }

        internal ReadOnlyDictionary<string, List<Triangle3>> Deserialize(string filePath)
        {
            return Deserialize(File.ReadAllBytes(filePath));
        }

        internal ReadOnlyDictionary<string, List<Triangle3>> Deserialize(byte[] data)
        {
            var allGeo = new Dictionary<string, List<Triangle3>>();

            using (MemoryStream ms = new MemoryStream(data))
            using (BinaryReader reader = new BinaryReader(ms))
            {
                int dictCount = reader.ReadInt32();

                for (int i = 0; i < dictCount; i++)
                {
                    string key = reader.ReadString();
                    int listCount = reader.ReadInt32();
                    var list = new List<Triangle3>();

                    for (int j = 0; j < listCount; j++)
                    {
                        Vector3 a = ReadVector3(reader);
                        Vector3 b = ReadVector3(reader);
                        Vector3 c = ReadVector3(reader);

                        list.Add(new Triangle3(a, b, c));
                    }

                    allGeo[key] = list;
                }
            }

            return new ReadOnlyDictionary<string, List<Triangle3>>(allGeo);
        }

        private Vector3 ReadVector3(BinaryReader reader)
        {
            float x = reader.ReadSingle();
            float y = reader.ReadSingle();
            float z = reader.ReadSingle();

            return new Vector3(x, y, z);
        }

        private static void WriteVector3(BinaryWriter writer, Vector3 vector)
        {
            writer.Write(vector.X);
            writer.Write(vector.Y);
            writer.Write(vector.Z);
        }
    }
}